using Godot;
using System;

public class Player: KinematicBody2D {
	public bool CanMove = true;
	public int Hp = 2;
	public int Deaths = 0;
	
	private int maxSpeed = 100;
	private int acceleration = 500;
	private int friction = 500;
	private Vector2 velocity = Vector2.Zero;
	
	private static Vector2 playerController (
		Vector2 velocity,
		int maxSpeed,
		int acceleration,
		int friction
	) {
		var velocityChangeVector = Vector2.Zero;

		velocityChangeVector.x = Input.GetActionStrength("right") - Input.GetActionStrength("left");
		velocityChangeVector.y = Input.GetActionStrength("bottom") - Input.GetActionStrength("top");

		if (velocityChangeVector == Vector2.Zero) {
			return velocity.MoveToward(Vector2.Zero, friction);
		}
		
		return velocity.MoveToward(velocityChangeVector.Normalized() * maxSpeed, acceleration);
	}
	
	public override void _Process(float delta) 
	{
		if (CanMove) {
			var newVelocityBeforeCollision = playerController(velocity, maxSpeed, acceleration, friction);
			velocity = MoveAndSlide(newVelocityBeforeCollision);
			
			if (Input.IsActionPressed("click") && gun != null)
				Fire();
			
			ProcessGunRotation();
			ProcessGunHide();
			ProcessPlayerRotation();
		}
	}
	
	public void HpDown() 
	{
		Hp--;
		if (Hp <= 0) 
		{
			GetTree().ReloadCurrentScene();
		}
	}
	
	private bool IsTurnedRight = true;
	
	private void ProcessPlayerRotation() {
		var WasTurnedRight = IsTurnedRight;
		
		var mouse = GetGlobalMousePosition().x;
		var playerPos = GlobalPosition.x;
		var isNeedToBeRight = (mouse - playerPos > 0);
		
		var directionShouldChange = (isNeedToBeRight != WasTurnedRight);
		
		if (directionShouldChange) {
			gun.ApplyScale(new Vector2(1, -1));
			((Sprite) GetNode("Sprite")).ApplyScale(new Vector2(-1, 1));
			IsTurnedRight = !IsTurnedRight;
		}
		
		//((Control) GetNode("Camera2D/UI")).ApplyScale(new Vector2(-1, 1));
	}
	
	public override void _Ready() {
		gun = (Gun) GetNode("Gun");
	}
	
	[Export]
	private String pathToBullets;
	
	public void ChangeWeapon(string weaponName) 
	{
		var weaponScene = (PackedScene)ResourceLoader
				.Load($"res://ObjectScenes/Weapons/{weaponName}.tscn");
		var newWeapon = (Gun)weaponScene.Instance();
		newWeapon.Transform = gun.Transform;
		gun.QueueFree();
		
		AddChild(newWeapon);
		gun = (Gun) GetNode(weaponName);
	}
	
	private Gun gun;
	
	private void Fire() {
		gun.Fire(pathToBullets);
	}
	
	private void ProcessGunHide() {
		var mouse = GetGlobalMousePosition().y;
		var playerPos = GlobalPosition.y - 12;
		
		var isNeedToBeHidden = mouse - playerPos < 0;
		
		if (isNeedToBeHidden != gun.ShowBehindParent) {
			gun.ShowBehindParent = !gun.ShowBehindParent;
		}
	}
	
	private void ProcessGunRotation() {
		var mouse = GetGlobalMousePosition();
		gun.LookAt(mouse);
	}
}

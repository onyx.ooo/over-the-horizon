using Godot;
using System;

public class Bullet : Area2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private int speed = 500;
	private float distance = 600f;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		this.Connect("area_entered", this, "Collision");
	}
	
	
	public override void _PhysicsProcess(float delta) 
	{
 		Position += Transform.x * speed * delta;
		distance -= speed * delta;
		if (distance < 0)
			QueueFree();
	}
	
	public void Collision(Area2D area)
	{
		if (area.GetParent() is EnemyBody)
		{
			var enemy = (EnemyBody) area.GetParent();
			enemy.HpDown();
		}
		if (area.GetParent() is Player)
		{
			var player = (Player) area.GetParent();
			player.HpDown();
		}
		QueueFree();
	}
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}

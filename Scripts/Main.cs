using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Main: Node {
	RichTextLabel HpBar;
	RichTextLabel ObjectiveBar;
	RichTextLabel UserMessage;
	RichTextLabel UserMessage_PressEnterLabel;
	ColorRect UserMessage_Bg;
	Player player;
	
	Queue<string> AwaitedUserMessages = new Queue<string>();
	string currentMessage;
	bool WaitingForEnterPress = false;
	bool CanCloseUiNow = false;
	bool MessageBoxActive = false;
	bool CanDequeue = true;
	
	Node2D CurrentInteractableObject;
	Node2D InteractableObjectBackup;
	Action CurrentOnInteractAction = () => {};
	
	int RecordedUserDeaths = 0;

	public void SetHp (int hpValue) {
		HpBar.Text = $"HP: { hpValue }";
	}
	
	public void SetObjective (string objectiveText) {
		ObjectiveBar.Text = objectiveText;
	}
	
	public void HideUserCursor () {
		Input.SetMouseMode(Input.MouseMode.Hidden);
	}
	
	public void ShowUserCursor () {
		Input.SetMouseMode(Input.MouseMode.Visible);
	}
	
	public void ShowUserMessage (string messageText) {
		player.CanMove = false;
		HideUserCursor();
		HpBar.Hide();
		ObjectiveBar.Hide();
		
		UserMessage.Show();
		UserMessage_Bg.Show();
		UserMessage_PressEnterLabel.Show();
		
		UserMessage.Text = messageText;
		MessageBoxActive = true;
	}
	
	public void RewriteUserMessage (string messageText) {
		UserMessage.Text = messageText;
	}
	
	public void HideUserMessage () {
		ShowUserCursor();
		HpBar.Show();
		ObjectiveBar.Show();
		
		UserMessage.Hide();
		UserMessage_Bg.Hide();
		UserMessage_PressEnterLabel.Hide();
		
		player.CanMove = true;
		MessageBoxActive = false;
	}
	
	public void SendUserMessages (List<string> messages) {
		foreach (var message in messages)
			AwaitedUserMessages.Enqueue(message);
	}
	
	public override void _Ready () {
		// UI
		HpBar = (RichTextLabel) GetNode("YSort/Player2/Camera2D/UI/HP");
		ObjectiveBar = (RichTextLabel) GetNode("YSort/Player2/Camera2D/UI/MissionStatus");
		UserMessage = (RichTextLabel) GetNode("YSort/Player2/Camera2D/UI/TextMessage");
		UserMessage_PressEnterLabel = (RichTextLabel) GetNode("YSort/Player2/Camera2D/UI/PressEnterBtn");
		UserMessage_Bg = (ColorRect) GetNode("YSort/Player2/Camera2D/UI/TextMsgBg");
		
		// Player
		player = (Player) GetNode("YSort/Player2");
		
		// UI init
		HideUserMessage();

		HpBar.Hide();
		ObjectiveBar.Hide();

		SetObjective("Find and loot the blue truck");
		
		SendUserMessages(new List<string> {
			"> Welcome to PUDGE - 'Professional Universal Demolishing with Greatest Effectiveness' program.",
			"> So, you will now be tested.",
			"> Your objective is to find and clear a nearby-located area around the Tower.",
			"> Oh, and... You better firstly checked that blue truck we left for you.",
			"> Go across the road and turn left on the first intersection.",
			"> [Come as close as you can and press E/F to loot]"
		});
		
		CurrentInteractableObject = (Node2D) GetNode("YSort/Truck2");
		CurrentOnInteractAction = () => {
			player.ChangeWeapon("RifleGun");
			SendUserMessages(new List<string> {
				"OO POVEZLO POVEZLO"
			});
			SetObjective("Clear the area near Tower");
		};
	}
	
	public override void _Process(float delta) {
		// sync player hp with ui value
		SetHp(player.Hp);
		
		// sync player deaths with ui value
		// TODO: need a wrapper above Main.tscn to count user deaths
		
		// process ui being shown
		if (CanDequeue && !WaitingForEnterPress && AwaitedUserMessages.Count > 0) {
			currentMessage = AwaitedUserMessages.Dequeue();
			WaitingForEnterPress = true;
			CanDequeue = false;

			// if message ui was hidden
			if (!MessageBoxActive) {
				ShowUserMessage(currentMessage);
				WaitingForEnterPress = false;
				Task.Delay(400).ContinueWith(t => {
					WaitingForEnterPress = true;
					CanDequeue = true;
				});
			}
		}
		
		// process enter_press
		if (WaitingForEnterPress && Input.IsActionPressed("ui_accept")) {
			WaitingForEnterPress = false;
			
			if (CanCloseUiNow) {
				HideUserMessage();
			} else {
				if (AwaitedUserMessages.Count == 0) {
					CanCloseUiNow = true;
				}
				
				RewriteUserMessage(currentMessage);
				
				// a little delay so player wouldn't skip everything in <1s
				Task.Delay(400).ContinueWith(t => {
					WaitingForEnterPress = true;
					CanDequeue = true;
				});
			}
		}
		
		// proccess user - object interaction
		if (CurrentInteractableObject != null && Input.IsActionPressed("ui_interactable")) 
		{
			var distanceBetweenCursorAndInteractableObject = player.GlobalPosition.DistanceTo(CurrentInteractableObject.GlobalPosition);
			var canInteract = distanceBetweenCursorAndInteractableObject < 100;
			if (!canInteract)
				return;
				
			CurrentInteractableObject = null;
			CurrentOnInteractAction();
		}
	}
}

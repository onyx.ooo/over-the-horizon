using Godot;
using System;

public class Gun : Node2D
{
	[Export]
	protected PackedScene AmmoType;
	public Position2D pointer { get; private set;}
	
	protected float FireRate = 0.3f;
	protected float TimeFromLastShot = 0f;
	
	public virtual void Fire(String path) 
	{
		if (TimeFromLastShot > FireRate)
		{
			var b = (Bullet) AmmoType.Instance();
			GetTree().CurrentScene.GetNode(path).AddChild(b);
			b.Transform = pointer.GlobalTransform;
			TimeFromLastShot = 0;
		}
	}
	
	protected void BulletCharge(float delta) 
	{
		if (TimeFromLastShot <= FireRate)
			TimeFromLastShot += delta; 
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GunReady();
	}
	
	protected void GunReady() 
	{
		pointer = (Position2D) GetNode("Pointer");
	}
	
	
	
//  Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
	  BulletCharge(delta);
  }
}

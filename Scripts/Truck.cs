using Godot;
using System;

public class Truck : StaticBody2D {
	public override void _Ready() {
		var area = (Area2D) GetNode("HitBox");
		area.Connect("area_entered", this, "Collision");
	}
	
	public void Collision(Area2D area) {
		//QueueFree();
		/* TRUCK NEVER GONNA DIE */
	}
}

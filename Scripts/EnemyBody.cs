using Godot;
using System;
using static System.Math;

public class EnemyBody : KinematicBody2D
{
	[Export]
	private String PathToTarget;
	private Player Target;
	private bool HasGun;
	public Node2D Vision;
	private bool isAggressive;
	public PathFollow2D PathToFollow;
	private int Speed = 50;
	private int Hp = 2;
	
	public override void _Ready() {
		HasGun = GetNode<Gun>("Gun") != null;
		Target = GetTree().CurrentScene.GetNode<Player>(PathToTarget);
		Vision = GetNode<Node2D>("Gun/Vision");
		PathToFollow = GetParent() as PathFollow2D;
	}
	
	public void HpDown() 
	{
		isAggressive = true;
		Hp--;
		if (Hp <= 0) 
		{
			QueueFree();
		}
	}
	
	private void ProcessGunRotation(float delta) 
	{
		var gun = GetNode<EnemyGun>("Gun");
		var targetVec = (Target.GlobalPosition - GlobalPosition);
		var gunVec = gun.GlobalPosition - gun.pointer.GlobalPosition;
		var direction = gunVec.AngleTo(-targetVec);
		var rotation = Math.Sign(direction) * delta * 4f;
		if (Math.Abs(direction) > 0.05f)
			gun.Rotation += rotation;
		else {
			gun.Rotation += direction;
			gun.Fire("YSort/Bullets");
		}
	}

	private bool IsTurnedRight = true;
	
	private void CheckView() {
		foreach (var el in Vision.GetChildren()) {
			var ray = (RayCast2D) el;
			if (ray.IsColliding())
				isAggressive = true; 
		}
	}
	
	public void Follow(float delta) {
		var prepos = PathToFollow.GlobalPosition;
		PathToFollow.SetOffset(PathToFollow.GetOffset() + delta * Speed);
		
		var pos = PathToFollow.GlobalPosition;
		var angle = pos.AngleToPoint(prepos);
		var gun = GetNode<EnemyGun>("Gun");
		
		gun.Rotation = angle;
	}
	
	public void FollowTarget(float delta) 
	{
		var angle = GlobalPosition
			.AngleToPoint(Target.GlobalPosition);
		MoveAndSlide(
			new Vector2((float)Cos(angle),(float)Sin(angle))
			* Speed * -1
			);
	}

	public override void _Process(float delta) 	{
		if (HasGun && Target != null && isAggressive) {
			FollowTarget(delta);
			ProcessGunRotation(delta); 
		} else {
			CheckView();
			if (PathToFollow != null)
				Follow(delta);
		}
  	}
}

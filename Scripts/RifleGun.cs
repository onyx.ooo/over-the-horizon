using Godot;
using System;
using System.Threading.Tasks;

public class RifleGun: Gun
{
	enum AutomaticRifleTick { ODD, EVEN };
	AnimationPlayer animationPlayer = null;
	AnimationTree animationTree = null;
	AnimationNodeStateMachinePlayback animationPlayback = null;
	bool canShoot = true;
	
	AutomaticRifleTick currentAutomaticRifleTick = AutomaticRifleTick.EVEN;
	
	public override void _Ready() {
		RifleGunReady();
	}
	
	public void RifleGunReady () {
		GunReady();
		animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
		animationTree = GetNode<AnimationTree>("AnimationTree");
		animationPlayback = (AnimationNodeStateMachinePlayback) animationTree.Get("parameters/playback");
		
		// syncronizing fire rate and gun anim speed
		var shootingAnimation = animationPlayer.GetAnimation("shooting");
		var animationScale = shootingAnimation.Length / (FireRate * 2);
		animationTree.Set("parameters/shooting/TimeScale/scale", animationScale);
	}
	
	public override void Fire (string bulletBoxPath) {
		AutomaticRifleTick NextAutomaticRifleTick (AutomaticRifleTick currentTick) {
			if (currentTick == AutomaticRifleTick.ODD)
				return AutomaticRifleTick.EVEN;
			return AutomaticRifleTick.ODD;
		}
		
		if (canShoot) {
			canShoot = !canShoot;
			var fireRateInMs = (int) (FireRate * 1000);
			Task.Delay(fireRateInMs).ContinueWith(t => { canShoot = !canShoot; });
			
			TimeFromLastShot = 0;
			var b = (Bullet) AmmoType.Instance();
			GetTree().CurrentScene.GetNode(bulletBoxPath).AddChild(b);
			b.Transform = pointer.GlobalTransform;
			
			if (currentAutomaticRifleTick == AutomaticRifleTick.EVEN) {
				animationPlayback.Travel("shooting");
			}
			
			currentAutomaticRifleTick = NextAutomaticRifleTick(currentAutomaticRifleTick);
			
			Task.Delay(fireRateInMs).ContinueWith(t => {
				if (!Input.IsActionPressed("click")) {
					animationPlayback.Travel("idle");
					currentAutomaticRifleTick = AutomaticRifleTick.EVEN;
				}
			});
		}
	}
}

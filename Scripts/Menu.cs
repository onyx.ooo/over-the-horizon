using Godot;
using System;

public class Menu: Control
{
	public override void _Ready()
	{
		var button = (Button) GetNode("/root/Control/Menu/MenuColoredContainer/StartButton");
		path = button.ScenePath;
		button.Connect("pressed", this, "CS");
	}
	
	public String path;
	
	public void CS () {
		GetTree().ChangeScene(path);
	}
}
